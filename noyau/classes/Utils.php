<?php

namespace Noyau\Classes;

abstract class Utils {

  /*
  ---------------------------------------------
  TRAITEMENT DES CHAÎNES DE CARACTERES
  ---------------------------------------------
   */

  /**
   * tronquer une chaîne au premier espace
   * après un certain nombre de caractères
   * @param  string  $chaine
   * @param  integer $nbreCaracteres [valeur par défaut]
   * @return string
   */
  public static function tronquer(string $chaine, int $nbreCaracteres = 200) :string {
    if(strlen($chaine) > $nbreCaracteres):
      $positionEspace = strpos($chaine, ' ', $nbreCaracteres);
      return substr($chaine, 0, $positionEspace);
    else:
      return $chaine;
    endif;
  }

  /*
  ---------------------------------------------
  TRAITEMENT DES DATES
  ---------------------------------------------
   */

  /**
   * formater une date avec un format par défaut
   * @param  string $date
   * @param  string $format [fortmat par défaut]
   * @return string
   */
  public static function formater_date(string $date, string $format = 'D d M Y') :string {
    return date_format(date_create($date), $format);
  }


}
