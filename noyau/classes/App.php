<?php

namespace Noyau\Classes;

abstract class App {

  private static $_connexion = null, $_rootPublic, $_rootAdmin, $_start = false;

  // GETTERS
    public static function getConnexion() :\PDO {
      return SELF::$_connexion;
    }
    public static function getRootPublic() :string {
      return SELF::$_rootPublic;
    }
    public static function getRootAdmin() :string {
      return SELF::$_rootAdmin;
    }

  // SETTERS
    private static function setConnexion(){
      $dsn = "mysql:host=".DB_HOST.";dbname=".DB_NAME;
      $param = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

      try {
           SELF::$_connexion = new \PDO($dsn,DB_USER,DB_PWD,$param);
      }
      catch (\PDOException $e) {
           die("Problème de connexion à la base de données...");
      }

    }
    private static function setRootPublic(){
      $local_path = str_replace(basename($_SERVER['SCRIPT_NAME']) , '', $_SERVER['SCRIPT_NAME']);
      SELF::$_rootPublic = 'http://'
                    . $_SERVER['HTTP_HOST']
                    . $local_path;
    }
    private static function setRootAdmin(){
      SELF::$_rootAdmin = str_replace(FOLDER_PUBLIC, FOLDER_BACKOFFICE, SELF::$_rootPublic);
    }

    // AUTRES METHODES
      public static function start() {
        if(!SELF::$_start):
          session_start();
          SELF::setRootPublic();
          SELF::setRootAdmin();
          SELF::setConnexion();
          SELF::$_start = true;
        endif;
      }
      public static function close() {
        SELF::$_connexion = null;
      }

}
