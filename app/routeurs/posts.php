<?php
use App\Controleurs\PostsControleur;
/*
  ./app/routeurs/posts.php
  isset($_GET['posts'])
 */

switch ($_GET['posts']) {
  case 'show':
  /*
    DETAILS D'UN POST
    PATTERN: index.php?posts=show&id=xxx
    CTRL: Posts
    ACTION: show
   */
      $ctrl = new \App\Controleurs\PostsControleur();
      $ctrl->showAction($_GET['id']);
    break;
}
