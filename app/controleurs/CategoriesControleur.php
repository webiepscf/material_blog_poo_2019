<?php
/*
  ./app/controleurs/CategoriesControleur
 */

namespace App\Controleurs;
use App\Modeles\CategoriesGestionnaire;

class CategoriesControleur {

  public function indexAction(){
    // Je demande au modele la liste des categories
    $gestionnaire = new CategoriesGestionnaire();
    $categories = $gestionnaire->findAll();

    // Je charge directement la vue index
      include '../app/vues/categories/index.php';
  }

}
