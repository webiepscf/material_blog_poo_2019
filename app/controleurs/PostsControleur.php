<?php
/*
  ./app/controleurs/postsControleur
 */

namespace App\Controleurs;
use App\Modeles\Post;

class PostsControleur {

  public function indexAction(){
    // Je demande au modele la liste des posts
    $gestionnaire = new \App\Modeles\PostsGestionnaire();
    $posts = $gestionnaire->findAll();

    // Je charge la vue index dans $content1
    GLOBAL $content1, $title;
    $title = POSTS_INDEX_TITLE;
    ob_start();
      include '../app/vues/posts/index.php';
    $content1 = ob_get_clean();
  }

  public function showAction(int $id = 1) {
    // Je demande les infos du posts au gestionnaire
    $gestionnaire = new \App\Modeles\PostsGestionnaire();
    $post = $gestionnaire->findOneById($id);

    // Je charge la vue show dans $content1
      GLOBAL $content1, $title;
      $title = $post->getTitre();
      ob_start();
        include '../app/vues/posts/show.php';
      $content1 = ob_get_clean();
  }

}
