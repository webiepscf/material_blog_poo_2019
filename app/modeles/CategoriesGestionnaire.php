<?php
/*
  ./app/modeles/CategoriesGestionnaire
 */

namespace App\Modeles;
use \Noyau\Classes\App;

class CategoriesGestionnaire {

    public function findAll() :array {
      $sql = "SELECT *
              FROM categories
              ORDER BY titre ASC;";

      $rs = App::getConnexion()->query($sql);
      $categories = $rs->fetchAll(\PDO::FETCH_ASSOC);

      // Je transforme le tableau indexé de tableaux associatifs
      // En tableau indexé d'objets de type Categorie
      $tabObjets = [];
      foreach ($categories as $categorie) {
        $tabObjets[] = new Categorie($categorie);
      }
      return $tabObjets;
    }

}
