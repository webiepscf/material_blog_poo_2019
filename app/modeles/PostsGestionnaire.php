<?php
/*
  ./app/modeles/PostsGestionnaire
 */

namespace App\Modeles;
use \Noyau\Classes\App;

class PostsGestionnaire {

    public function findAll() :array {
      $sql = "SELECT *
              FROM posts
              ORDER BY datePublication DESC
              LIMIT 10;";

      $rs = App::getConnexion()->query($sql);
      $posts = $rs->fetchAll(\PDO::FETCH_ASSOC);

      // Je transforme le tableau indexé de tableaux associatifs
      // En tableau indexé d'objets de type Post
      $tabObjets = [];
      foreach ($posts as $post) {
        $tabObjets[] = new Post($post);
      }
      return $tabObjets;
    }

/**
 * [findOneById description]
 * @param  PDO     $connexion [description]
 * @param  integer $id        [description]
 * @return Post               [description]
 */
    public function findOneById(int $id = 1) :Post {
      $sql = "SELECT *
              FROM posts
              WHERE id = :id;";
      $rs = App::getConnexion()->prepare($sql);
      $rs->bindValue(':id', $id, \PDO::PARAM_INT);
      $rs->execute();
      return new Post($rs->fetch(\PDO::FETCH_ASSOC));
    }

}
