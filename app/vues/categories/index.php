<?php
/*
  ./app/vues/categories/index.php
  Variables disponibles :
    - $categories ARRAY(Categorie(id, titre, slug))
 */
 ?>
 <h4>Categories</h4>

 <ul class="collection">
<?php foreach ($categories as $categorie): ?>
  <li>
    <a href="categorie/bidon" class="collection-item">
      <?php echo $categorie->getTitre(); ?>    
    </a>
  </li>
<?php endforeach; ?>
 </ul>
