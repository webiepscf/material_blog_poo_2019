<?php
/*
  ./app/vues/posts/show.php
  Variables disponibles :
    - $post POST(id, titre, texte, datePublication, ...)
 */
 ?>
 <!-- Title -->
 <h1><?php echo $post->getTitre(); ?></h1>

 <!-- Author -->
 <p class="lead">
   by <a href="#"><?php echo $post->getAuteur(); ?></a>
 </p>

 <hr>

 <!-- Date/Time -->
 <p>Posted on
   <?php echo \Noyau\Classes\Utils::formater_date($post->getdatePublication()); ?></p>

 <hr>

 <!-- Preview Image -->
 <img class="img-responsive z-depth-2" src="<?php echo $post->getMedia(); ?>" alt="">

 <hr>

 <!-- Post Content -->
 <div><?php echo $post->getTexte(); ?></div>


 <hr>
