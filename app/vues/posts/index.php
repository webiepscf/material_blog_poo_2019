<?php
/*
  ./app/vues/posts/index.php
  Variables disponibles :
    - $posts ARRAY(POST(id, titre, texte, datePublication, ...))
 */
 ?>

 <h1 class="page-header">
     Material Design for Bootstrap
     <small>made with love</small>
 </h1>

 <?php foreach ($posts as $post): ?>
   <article class="">
     <h2>
         <a href="posts/<?php echo $post->getId(); ?>/<?php echo $post->getSlug(); ?>"><?php echo $post->getTitre(); ?></a>
     </h2>
     <p class="lead">
       by <a href="#">pascal</a>
     </p>
     <p> Posted on
       <?php echo \Noyau\Classes\Utils::formater_date($post->getDatePublication()); ?>     </p>
     <hr>
        <div><?php echo \Noyau\Classes\Utils::tronquer($post->getTexte()); ?></div>
     <a href="posts/<?php echo $post->getId(); ?>/<?php echo $post->getSlug(); ?>">
       <button type="button" class="btn btn-info waves-effect waves-light">Read more</button>
     </a>
     <hr>
   </article>
 <?php endforeach; ?>
