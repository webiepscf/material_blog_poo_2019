<?php
use App\Modeles\PostsGestionnaire;
/*
  ./app/routeur.php
 */


if (isset($_GET['posts'])):
  include_once '../app/routeurs/posts.php';

else:
  /*
    ROUTE PA DEFAUT
    PATTERN: /
    CTRL: Posts
    ACTION: index
   */
  $ctrl = new \App\Controleurs\PostsControleur();
  $ctrl->indexAction();
endif;
